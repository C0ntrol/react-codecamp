const axios = require("axios");

function getRooms() {
    const getRooms = axios.get(process.env.PUBLIC_URL + "/room-list.json");

    return getRooms.then(response => {
        return response.data;
    });
}

function getGraphInfo() {
	const getGraphInfo = axios.get(process.env.PUBLIC_URL + "/graph-list.json");

    return getGraphInfo.then(response => {
        return response.data.graphs;
    });

}

module.exports = {
    getRooms,
    getGraphInfo
};