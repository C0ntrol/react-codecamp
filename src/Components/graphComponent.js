import React, {Component} from 'react';
import daily from "../img/dailyUsage.png";
import power from "../img/powerUsage.png";


class GraphComponent extends Component {
    constructor(props) {
        super(props);
        this.itemClicked = this.itemClicked.bind(this);
        this.state = {
            image: daily
        }

    }

    itemClicked(graph) {
        if (graph.graphName === 'Last Used') {
            this.setState({image: daily});
        }
        else {
            this.setState({image: power});
        }
    }

    render() {
        const graphs = this.props.graphs;
        const roomNames = this.props.roomNames;
        if (graphs) {
            if (!roomNames) {
                return null;
            } else {
                return (
                    <div className="container-fluid">
                        <h1>{roomNames}</h1>
                        <ul className="nav navbar-nav">
                            {graphs.map(graph => <li className="nav-item" key={graph.graphId}
                                                     onClick={() => this.itemClicked(graph)}>{graph.graphName}</li>)}
                        </ul>
                        <img src={this.state.image} alt="img"/>
                    </div>
                );
            }
        }
        else {
            return <div>No power data available</div>
        }

    }
}

export default GraphComponent;