import React, {Component} from 'react';
import SchematicIconComponent from './SchematicIconComponent.js';
import './SchematicsComponent.css';

class SchematicComponent extends Component {
    render() {
        if (this.props.rooms) {
            return (
                <div className="border border-dark schematics-component-wrapper"> {this.props.rooms.map(room =>
                    <div key={room.roomId} style={room.location} className="room d-flex flex-column justify-content-center align-items-center hand-cursor" onClick={() => this.props.changeIsActiveStatus(room.roomId, room.type)}>
                        <div>
                            <span className="font-weight-bold noselect">{room.label}</span>
                        </div>
                        <div>
                            {room.controllables.map(controllable => {
                                if (controllable.isPowered) {
                                    return <SchematicIconComponent key={controllable.controllableId} controllable={controllable}/>
                                } else {
                                    return null;
                                }
                            })}
                        </div>
                    </div>)}
                </div>
            );
        } else {
            return (
                <div
                    className="border border-dark schematics-component-wrapper d-flex justify-content-center align-items-center">
                    <div>No rooms :(</div>
                </div>

            )
        }
    }
}

export default SchematicComponent;