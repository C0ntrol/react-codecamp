This project was created for Lappeenranta University of Technology's React.JS Codecamp in January 2017.

##Compatibility
- Chrome, Firefox

##Install
- Clone the main repository
- `npm install`
- `npm start`
- Open browser and navigate [here](http://localhost:3000).
